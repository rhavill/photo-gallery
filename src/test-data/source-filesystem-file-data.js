const fileData = {
  photos: {
    nodes: [
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/1.jpg 1x',
          src: '/static/blah/blah/1.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: '1.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/2.jpg 1x',
          src: '/static/blah/blah/2.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/2.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/3.jpg 1x',
          src: '/static/blah/blah/3.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/3.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/4.jpg 1x',
          src: '/static/blah/blah/4.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/another-level-two/4.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/5.jpg 1x',
          src: '/static/blah/blah/5.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/5.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/6.jpg 1x',
          src: '/static/blah/blah/6.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/6.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/7.jpg 1x',
          src: '/static/blah/blah/7.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/7.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/8.jpg 1x',
          src: '/static/blah/blah/8.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/8.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/9.jpg 1x',
          src: '/static/blah/blah/9.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/9.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/10.jpg 1x',
          src: '/static/blah/blah/10.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/10.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/11.jpg 1x',
          src: '/static/blah/blah/11.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/11.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/12.jpg 1x',
          src: '/static/blah/blah/12.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/12.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/13.jpg 1x',
          src: '/static/blah/blah/13.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/13.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/14.jpg 1x',
          src: '/static/blah/blah/14.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/14.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/15.jpg 1x',
          src: '/static/blah/blah/15.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/15.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/16.jpg 1x',
          src: '/static/blah/blah/16.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/16.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/17.jpg 1x',
          src: '/static/blah/blah/17.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/17.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/18.jpg 1x',
          src: '/static/blah/blah/18.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/18.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/19.jpg 1x',
          src: '/static/blah/blah/19.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/19.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/20.jpg 1x',
          src: '/static/blah/blah/20.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/20.jpg'},
      {childImageSharp: {
        fixed: {
          srcSet: '/static/blah/blah/21.jpg 1x',
          src: '/static/blah/blah/21.jpg',
          width: 250,
          height: 250
        }
      },
      relativePath: 'level-one/level-two/level-three/21.jpg'},
    ]
  }
}

export default fileData