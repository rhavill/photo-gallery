import imageData from '../test-data/gatsby-image-data'

const folderData = {
  folderIcon: imageData,
  folders: {
    nodes: [
      {relativePath: ''},
      {relativePath: 'level-one'},
      {relativePath: 'another-level-one'},
      {relativePath: 'yet-another-level-one'},
      {relativePath: 'level-one/level-two'},
      {relativePath: 'level-one/another-level-two'},
    ]
  }
}

export default folderData