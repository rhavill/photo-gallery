const queryResults = `{
  "data": {
    "site": {
      "siteMetadata": {
        "photosPerPage": 15
      }
    },
    "folders": {
      "nodes": [
        {
          "relativePath": ""
        },
        {
          "relativePath": "2019-puerto-rico"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan"
        }
      ]
    },
    "photos": {
      "nodes": [
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_113735817-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_113735817-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_102958452-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_102958452-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103213316-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_103213316-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103224765-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_103224765-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103246832-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_103246832-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103432382-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_103432382-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_104325636-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_104325636-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_113817195-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_113817195-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_102840214-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_102840214-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103147506-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_103147506-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_115110329-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_115110329-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_115113322-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_115113322-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190411_184245_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190411_184245_vHDR_Auto-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190412_061017_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190412_061017_vHDR_Auto-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_102530976-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_102530976-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_102644839-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_102644839-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103038266-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_103038266-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103131232-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_103131232-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/IMG_20190801_180122560_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "IMG_20190801_180122560_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190624_194805_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190624_194805_vHDR_Auto-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_080312072_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_080312072_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_080948943_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_080948943_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_092941306-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_092941306-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_102725343-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_102725343-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_114723977_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_114723977_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190411_184337_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190411_184337_vHDR_Auto-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190418_101747_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190418_101747_vHDR_Auto-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190430_110713_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190430_110713_vHDR_Auto-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190510_181351_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190510_181351_vHDR_Auto-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190624_195311_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190624_195311_vHDR_Auto-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_080329469_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_080329469_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_094630551_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_094630551_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_114710944_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_114710944_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_094059100_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_094059100_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_100222665_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_100222665_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_100617298_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_100617298_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_105036716_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_105036716_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_115014012_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_115014012_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190416_105040_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190416_105040_vHDR_Auto-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_094105091_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_094105091_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_094312229_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_094312229_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_094342843_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_094342843_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103014691-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_103014691-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_104939430_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_104939430_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_113700074_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_113700074_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_114940243_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_114940243_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_115222712_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_115222712_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_080637486_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_080637486_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_113614522_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_113614522_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190411_101018_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190411_101018_vHDR_Auto-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_085859250-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_085859250-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_094243588_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_094243588_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_113548617_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_113548617_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_114002640_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_114002640_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190411_101034_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190411_101034_vHDR_Auto-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_085902417_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_085902417_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_132357515_HDR-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_132357515_HDR-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_132347391-small.jpg",
          "relativeDirectory": "2019-puerto-rico/jayuya",
          "base": "IMG_20190814_132347391-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190429_122557_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190429_122557_vHDR_Auto-small.jpg"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190416_112405_vHDR_Auto-small.jpg",
          "relativeDirectory": "2019-puerto-rico/san-juan",
          "base": "P_20190416_112405_vHDR_Auto-small.jpg"
        }
      ]
    }
  }
}`

module.exports = JSON.parse(queryResults)