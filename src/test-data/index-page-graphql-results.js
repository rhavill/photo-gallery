const data = `
{
  "data": {
    "site": {
      "pathPrefix": "/path-prefix"
    },
    "photos": {
      "nodes": [
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_113735817-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/bd5e73a4e4ef1e6c999f05a63948fb15/284df/IMG_20190814_113735817-small.jpg",
              "srcSet": "/static/bd5e73a4e4ef1e6c999f05a63948fb15/284df/IMG_20190814_113735817-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_102958452-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/dab4df681d51b31f5b99e054a3f4f887/284df/IMG_20190814_102958452-small.jpg",
              "srcSet": "/static/dab4df681d51b31f5b99e054a3f4f887/284df/IMG_20190814_102958452-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103246832-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/aefd66f4e68e248199c7225bd3ec8948/284df/IMG_20190814_103246832-small.jpg",
              "srcSet": "/static/aefd66f4e68e248199c7225bd3ec8948/284df/IMG_20190814_103246832-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103224765-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/17f384355409c815d6279f4d0c6c33a5/284df/IMG_20190814_103224765-small.jpg",
              "srcSet": "/static/17f384355409c815d6279f4d0c6c33a5/284df/IMG_20190814_103224765-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103432382-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/76540254356b3dad5c7347782b649d5b/284df/IMG_20190814_103432382-small.jpg",
              "srcSet": "/static/76540254356b3dad5c7347782b649d5b/284df/IMG_20190814_103432382-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_104325636-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/097fee76750829e6093dd01b5cf77350/284df/IMG_20190814_104325636-small.jpg",
              "srcSet": "/static/097fee76750829e6093dd01b5cf77350/284df/IMG_20190814_104325636-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103213316-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/62d1e49090d0f8a33026172e988f62ef/284df/IMG_20190814_103213316-small.jpg",
              "srcSet": "/static/62d1e49090d0f8a33026172e988f62ef/284df/IMG_20190814_103213316-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_113817195-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/1d5338456eb7a30531121a2e30aa10ad/284df/IMG_20190814_113817195-small.jpg",
              "srcSet": "/static/1d5338456eb7a30531121a2e30aa10ad/284df/IMG_20190814_113817195-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_102840214-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/af4bd77844ec49e397372ff56c31b777/284df/IMG_20190814_102840214-small.jpg",
              "srcSet": "/static/af4bd77844ec49e397372ff56c31b777/284df/IMG_20190814_102840214-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_103147506-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/75c23ad687b56882d8737134347f026c/284df/IMG_20190814_103147506-small.jpg",
              "srcSet": "/static/75c23ad687b56882d8737134347f026c/284df/IMG_20190814_103147506-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_115110329-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/207c49757789a1df2989b3eecbeff21e/284df/IMG_20190814_115110329-small.jpg",
              "srcSet": "/static/207c49757789a1df2989b3eecbeff21e/284df/IMG_20190814_115110329-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/jayuya/IMG_20190814_115113322-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/d54a075bc9f5dd6832c8c18117162745/284df/IMG_20190814_115113322-small.jpg",
              "srcSet": "/static/d54a075bc9f5dd6832c8c18117162745/284df/IMG_20190814_115113322-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "IMG_20190810_110416612-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/19a9a70311ba610371ad80faa5597a73/284df/IMG_20190810_110416612-small.jpg",
              "srcSet": "/static/19a9a70311ba610371ad80faa5597a73/284df/IMG_20190810_110416612-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190412_061017_vHDR_Auto-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/5bc0ef1c9193450ed44ecb462346efcf/284df/P_20190412_061017_vHDR_Auto-small.jpg",
              "srcSet": "/static/5bc0ef1c9193450ed44ecb462346efcf/284df/P_20190412_061017_vHDR_Auto-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        },
        {
          "relativePath": "2019-puerto-rico/san-juan/P_20190411_184245_vHDR_Auto-small.jpg",
          "childImageSharp": {
            "fixed": {
              "src": "/static/9af73340f7cbeb7526da89e190d55e8b/284df/P_20190411_184245_vHDR_Auto-small.jpg",
              "srcSet": "/static/9af73340f7cbeb7526da89e190d55e8b/284df/P_20190411_184245_vHDR_Auto-small.jpg 1x",
              "width": 250,
              "height": 250
            }
          }
        }
      ]
    },
    "folderIcon": {
      "childImageSharp": {
        "fixed": {
          "src": "/static/b75d8bd08d0e30f3925cc6187ddc8b84/f17d3/folder.png",
          "srcSet": "/static/b75d8bd08d0e30f3925cc6187ddc8b84/f17d3/folder.png 1x",
          "width": 250,
          "height": 250
        }
      }
    },
    "folders": {
      "nodes": [
        {
          "relativePath": ""
        },
        {
          "relativePath": "2019-puerto-rico"
        },
        {
          "relativePath": "2019-puerto-rico/jayuya"
        },
        {
          "relativePath": "2019-puerto-rico/san-juan"
        },
        {
          "relativePath": ""
        }
      ]
    }
  }
}`

module.exports = JSON.parse(data)